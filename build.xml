<?xml version="1.0" encoding="UTF-8"?>
<project default="full-build" name="animeclient" basedir=".">
	<!-- By default, we assume all tools to be on the $PATH -->
	<property name="pdepend" value="pdepend" />
	<property name="phpcpd" value="phpcpd" />
	<property name="phpdox" value="phpdox" />
	<property name="phploc" value="phploc" />
	<property name="phpmd" value="phpmd" />
	<property name="phpunit" value="phpunit" />
	<property name="sonar" value="sonar-runner" />

	<target name="full-build"
		depends="prepare,static-analysis,phpunit,phpdox,sonar"
		description="Performs static analysis, runs the tests, and generates project documentation"
	/>
	<target name="full-build-parallel"
		depends="prepare,static-analysis-parallel,phpunit,phpdox"
		description="Performs static analysis (executing the tools in parallel), runs the tests, and generates project documentation"
	/>
	<target name="quick-build"
		depends="prepare,lint,phpunit-no-coverage"
		description="Performs a lint check and runs the tests (without generating code coverage reports)"
	/>
	<target name="static-analysis"
		depends="lint,phploc-ci,pdepend,phpcpd-ci"
		description="Performs static analysis"
	/>

	<!-- Adjust the threadCount attribute's value to the number of CPUs -->
	<target name="static-analysis-parallel" description="Performs static analysis (executing the tools in parallel)">
		<parallel threadCount="6">
			<sequential>
				<antcall target="pdepend" />
			</sequential>
			<antcall target="lint" />
			<antcall target="phpcpd-ci" />
			<antcall target="phploc-ci" />
		</parallel>
	</target>

	<target name="clean" unless="clean.done" description="Cleanup build artifacts">
		<delete dir="build/api" />
		<delete dir="build/coverage" />
		<delete dir="build/logs" />
		<delete dir="build/pdepend" />
		<delete dir="build/phpdox" />
		<property name="clean.done" value="true" />
	</target>

	<target name="prepare" depends="clean" unless="prepare.done" description="Prepare for build">
		<mkdir dir="build/api" />
		<mkdir dir="build/coverage" />
		<mkdir dir="build/logs" />
		<mkdir dir="build/pdepend" />
		<mkdir dir="build/phpdox" />
		<property name="prepare.done" value="true" />
	</target>

	<target name="lint" unless="lint.done" description="Perform syntax check of sourcecode files">
		<apply executable="php" taskname="lint">
			<arg value="-l" />

			<fileset dir="src">
				<include name="**/*.php" />
			</fileset>

			<fileset dir="tests">
				<include name="**/*.php" />
			</fileset>
		</apply>

		<property name="lint.done" value="true" />
	</target>

	<target name="phploc" unless="phploc.done" description="Measure project size using PHPLOC and print human readable output. Intended for usage on the command line.">
		<exec executable="${phploc}" taskname="phploc">
			<arg value="--count-tests" />
			<arg path="src" />
			<arg path="tests" />
		</exec>

		<property name="phploc.done" value="true" />
	</target>

	<target name="phploc-ci" depends="prepare" unless="phploc.done" description="Measure project size using PHPLOC and log result in CSV and XML format. Intended for usage within a continuous integration environment.">
		<exec executable="${phploc}" taskname="phploc">
			<arg value="--count-tests" />
			<arg value="--log-csv" />
			<arg path="build/logs/phploc.csv" />
			<arg value="--log-xml" />
			<arg path="build/logs/phploc.xml" />
			<arg path="src" />
			<arg path="tests" />
		</exec>

		<property name="phploc.done" value="true" />
	</target>

	<target name="pdepend" depends="prepare" unless="pdepend.done" description="Calculate software metrics using PHP_Depend and log result in XML format. Intended for usage within a continuous integration environment.">
		<exec executable="${pdepend}" taskname="pdepend">
			<arg value="--jdepend-xml=build/logs/jdepend.xml" />
			<arg value="--jdepend-chart=build/pdepend/dependencies.svg" />
			<arg value="--overview-pyramid=build/pdepend/overview-pyramid.svg" />
			<arg path="src" />
		</exec>

		<property name="pdepend.done" value="true" />
	</target>

	<target name="phpcpd" unless="phpcpd.done" description="Find duplicate code using PHPCPD and print human readable output. Intended for usage on the command line before committing.">
		<exec executable="${phpcpd}" taskname="phpcpd">
			<arg path="src" />
		</exec>

		<property name="phpcpd.done" value="true" />
	</target>

	<target name="phpcpd-ci" depends="prepare" unless="phpcpd.done" description="Find duplicate code using PHPCPD and log result in XML format. Intended for usage within a continuous integration environment.">
		<exec executable="${phpcpd}" taskname="phpcpd">
			<arg value="--log-pmd" />
			<arg path="build/logs/pmd-cpd.xml" />
			<arg path="src" />
		</exec>

		<property name="phpcpd.done" value="true" />
	</target>

	<target name="phpunit" unless="phpunit.done" depends="prepare" description="Run unit tests with PHPUnit">
		<exec executable="${phpunit}" taskname="phpunit">
			<arg value="--configuration" />
			<arg path="build/phpunit.xml" />
		</exec>

		<property name="phpunit.done" value="true" />
	</target>

	<target  name="phpunit-no-coverage" depends="prepare" unless="phpunit.done" description="Run unit tests with PHPUnit (without generating code coverage reports)">
		<exec executable="${phpunit}" failonerror="true" taskname="phpunit">
			<arg value="--configuration" />
			<arg path="build/phpunit.xml" />
			<arg value="--no-coverage" />
		</exec>

		<property name="phpunit.done" value="true" />
	</target>

	<target name="phpdox" depends="phploc-ci,phpunit" unless="phpdox.done" description="Generate project documentation using phpDox">
		<exec dir="build" executable="${phpdox}" taskname="phpdox" />

		<property name="phpdox.done" value="true" />
	</target>

	<target name="sonar" depends="phpunit" unless="sonar.done" description="Generate code analysis with sonarqube">
		<exec executable="${sonar}" taskname="sonar" />

		<property name="sonar.done" value="true" />
	</target>
</project>