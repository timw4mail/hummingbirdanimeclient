<?php
/**
 * Ion
 *
 * Building blocks for web development
 *
 * @package     Ion
 * @author      Timothy J. Warren
 * @copyright   Copyright (c) 2015 - 2016
 * @license     MIT
 */

namespace Aviat\Ion\Di\Exception;

/**
 * Generic exception for Di Container
 */
class ContainerException extends \Exception implements \Interop\Container\Exception\ContainerException {

}
// End of ContainerException.php